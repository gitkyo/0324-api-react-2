import Navigation from "../component/nav";
import TodoList from "../component/todos";
// import { getTodos } from "../todos";


// export async function loader() {
//     const todos = await getTodos();
//     return { todos };
//   }


export default function TodosRoute() {
    // const { todos } = useLoaderData();
    // const { todos } = {
    //     description: "This is a description",
    //     completed: false,
    //     id: 1,
    // }
    return (
      <>
        <div id="sidebar">
          <h1>React TodoList App</h1>  
          <h2>Todos !</h2>        
          <Navigation />
        </div>     
        <TodoList />

      </>
    );
  }