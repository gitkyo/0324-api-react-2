import Navigation from "../component/nav";
import Home from "../component/home";
// import { getTodos } from "../todos";


// export async function loader() {
//     const todos = await getTodos();
//     return { todos };
//   }


export default function Root() {
    // const { todos } = useLoaderData();
    // const { todos } = {
    //     description: "This is a description",
    //     completed: false,
    //     id: 1,
    // }
    return (
      <>
        <div id="sidebar">
          <h1>React TodoList App</h1>  
          <h2>Welcome !</h2>        
          <Navigation />
        </div>     
        <Home />

      </>
    );
  }