import React from 'react';
import Navigation from "../component/nav";
import Contact from "../component/contact";

function ContactRoute() {

  return (
    <>
      <div id="sidebar">
        <h1>React TodoList App</h1>  
        <h2>Contact !</h2>        
        <Navigation />
      </div>        
      <Contact/>    
    </>
  );
}

export default ContactRoute;
