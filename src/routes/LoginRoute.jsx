
import React, { useState } from 'react';
//import login
import Login from '../component/login.jsx';
import Navigation from '../component/nav.jsx';


function LoginRoute() {

  return (
  <>
      <div id="sidebar">
          <h1>React TodoList App</h1>  
          <h2>Welcome !</h2>        
          <Navigation />
      </div>   
      <Login />
  </>
  );
}

export default LoginRoute;
