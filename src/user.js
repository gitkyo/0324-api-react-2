
class UserService {
  constructor(apiUrl) {
    this.apiUrl = apiUrl;
  }

  async login(user) {
    const email = user.email;
    const password = user.password;
    const response = await fetch(`${this.apiUrl}/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'credentials': 'include'
      },
      body: JSON.stringify({ email, password })
    });
     

    if (!response.ok) {
      throw new Error('Failed to login');
    }

    //test if a cookie with token key exist    
    console.log('response',response.headers.get('set-cookie'));    


    const data = await response.json();
    return data;
  }

  async logout(token) {
    const response = await fetch(`${this.apiUrl}/logout`, {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    });

    if (!response.ok) {
      throw new Error('Failed to logout');
    }
  }
}

export default UserService;
