import localforage from "localforage";
import { matchSorter } from "match-sorter";
import sortBy from "sort-by";


// const urlApi = process.env.urlApi
const urlApi = import.meta.env.VITE_API_URL

export async function getTodos() {

  try {   

    //test if todos exist in localstorage
    const todos = await localforage.getItem("todos");

    if (!todos) {
      
      //get token from cookies
      const token = document.cookie.split('=')[1]    

      const response = await fetch(`${urlApi}/tasks`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
      })

      if (!response.ok) {
        throw new Error('Failed to get todos');
      }
      
      const data = await response.json()

      //store in localstorage
      await localforage.setItem("todos", data);
      console.log('todos from API');
      return data
    }else{
      //return todos from localstorage

      console.log('todos from localstorage');
      return todos
    }
  } catch (error) {
      console.log(error)
  }
}

export async function createTodo(todo) {

  try {

    //get token from cookies
    const token = document.cookie.split('=')[1]    

    const response = await fetch(`${urlApi}/tasks`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify(todo)
    })

    if (!response.ok) {
      throw new Error('Failed to create todo');
    }
    
    const data = await response.json()

    //adding this todos in todolist in localstorage
    let todos = await localforage.getItem("todos");
    todos.push(data);
    await localforage.setItem("todos", todos);   
    
    return data


  } catch (error) {
    console.log(error)
  }
  
}

export async function getTodo(id) {
  await fakeNetwork(`todo:${id}`);
  let todos = await localforage.getItem("todos");
  let todo = todos.find(todo => todo.id === id);
  return todo ?? null;
}

export async function updateTodo(id, updates) {
  await fakeNetwork();
  let todos = await localforage.getItem("todos");
  let todo = todos.find(todo => todo.id === id);
  if (!todo) throw new Error("No todo found for", id);
  Object.assign(todo, updates);
  await set(todos);
  return todo;
}

export async function deleteTodoService(todoId) {

  try {
      
     

      //get token from cookies
      const token = document.cookie.split('=')[1]    
  
      const response = await fetch(`${urlApi}/tasks/${todoId}`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
      })
  
      if (!response.ok) {
        throw new Error('Failed to delete todo');
      }
         
      let todos = await localforage.getItem("todos");
      todos = todos.filter(todo => todo.id !== todoId);      
      await localforage.setItem("todos", todos);   
      todos = await localforage.getItem("todos")

      console.log('todos', todos);
      
      return todos    
  } catch (error) {
      console.log(error);
  }
  
}

function set(todos) {
  return localforage.setItem("todos", todos);
}

// fake a cache so we don't slow down stuff we've already seen
let fakeCache = {};

async function fakeNetwork(key) {
  if (!key) {
    fakeCache = {};
  }

  if (fakeCache[key]) {
    return;
  }

  fakeCache[key] = true;
  return new Promise(res => {
    setTimeout(res, Math.random() * 800);
  });
}