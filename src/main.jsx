import * as React from "react";
import * as ReactDOM from "react-dom/client";
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import "./index.css";

import ErrorPage from "./error-page";
import Root from "./routes/root";
import ContactRoute from "./routes/contactRoute";
import LoginRoute from "./routes/LoginRoute";
import TodosRoute from "./routes/todosRoute";
//load env variables
// console.log(import.meta.env.VITE_API_URL) 

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    errorElement: <ErrorPage />, 
    // loader: rootLoader,


    // children: [
    //   {
    //     path: "/contact",
    //     element: <Contact />,

    //   }, 
    // ],
  },
  {
    path: "/contact",
    element: <ContactRoute />,
    errorElement: <ErrorPage />,
  },
  {
    path: "/login",
    element: <LoginRoute />,
    errorElement: <ErrorPage />,
    // children: [
    //   {
    //     path: "/todos",
    //     element: <TodosRoute />,

    //   }, 
    // ],
  },
  {
    path: "/todos",
    element: <TodosRoute />,
    errorElement: <ErrorPage />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);