import React from 'react';
import {Link} from 'react-router-dom';
import { useEffect } from 'react';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function Home() {

  const [isLogin, setIsLogin] = useState(false);
  const navigate = useNavigate();

  const checkLogin = () => {
    // if cookie with token
    const cookies = document.cookie.split(';');
    let tokenCookie = cookies.find((cookie) => cookie.includes('token'));
   
    if (localStorage.getItem('user') || tokenCookie) {
      return true;
    }
    return false;
  }
 
  useEffect(() => {      
    console.log('user Effect');
    const urlParams = new URLSearchParams(window.location.search);
    const token = urlParams.get('token');
    if (token) {
      console.log('token', token);
      //remove older cookie
      document.cookie = "token=; path=/; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
      //set token in cookie
      document.cookie = `token=${token}; path=/;`;
           
      setIsLogin(true); 
      navigate('/todos', { replace: true });
    }
    
  
  } , [isLogin]);

  return (
    <div>
        <h1>Welcome to My Todo List App</h1>
        <p>Here you can create and manage your daily tasks.</p>      
        {/* if is login */}
        {checkLogin() && (
          <Link to="/todos">
            <button type="button">Click here to see your tasks</button>
          </Link>
        )}
        {/* if not login */}
        {!checkLogin() && (
          <Link to="/login">
            <button type="button">Click here to connect</button>
          </Link>
        )}        
    </div>
  );
}

export default Home;
