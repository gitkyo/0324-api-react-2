
import React, { useState } from 'react';
import {getTodos, createTodo, deleteTodoService} from '../todos';
import { useEffect } from 'react';

function TodoList() {  

  const [todos, setTodos] = useState();

  //useEffect to get todos
  useEffect(() => {
    if (!checkLogin()) {
      window.location.href = '/';
    }else{
      getTodos().then((todos) => {
        setTodos(todos);      
      })   
    }
  }, []);

 
  const checkLogin = () => {
    // if cookie with token
    const cookies = document.cookie.split(';');
    let tokenCookie = cookies.find((cookie) => cookie.includes('token'));
   
    if (localStorage.getItem('user') || tokenCookie) {
      return true;
    }
    return false;
  }


  function addTodo(todo) {
    createTodo(todo).then((todo) => {      
      setTodos([...todos, todo]);
    })    
  }

  function deleteTodo(todoId) {     

    deleteTodoService(todoId).then((todo) => {
      // setTodos([...todos, todo]);
      const newTodos = todos.filter((todo) => todo.id !== todoId);

      console.log('newTodos', newTodos);
      setTodos(newTodos);

      
      
    })
    
  }

  function handleSubmit(event) {
    event.preventDefault();
    
    
    const todoText = event.target.elements.todoText.value;
    const todoCompleted = event.target.elements.todoCompleted.checked;
    const todo = { description: todoText, completed: todoCompleted };
    console.log(todo);
    addTodo(todo);
    event.target.reset();
  }




  return (    
    <>
      

      <div>
        <h1>Todo List</h1>
        <form onSubmit={handleSubmit}>
          <input type="text" name="todoText" />          
          <input type="checkbox" name="todoCompleted" />
          <button type="submit">Add Todo</button>
        </form>
        <ul>
          
          
          {todos && todos.map((todo) => (

            
            <li key={todo.id}  >
              <input type="checkbox"  checked={todo.completed ? "checked" : ""} /> &nbsp;&nbsp;
              {todo.description}
              <button onClick={() => deleteTodo(todo.id)}>&times;</button>
            </li>
          ))}  
            


              
        </ul>
      </div>
    </>
  );
}

export default TodoList;
