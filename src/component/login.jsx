
import React, { useState } from 'react';
import UserService from './../user';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';

function Login() {

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  //isLogin is a boolean to check if user is logged in or not
  const [isLogin, setIsLogin] = useState(false);

  const handleUsernameChange = (event) => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };
  
  const navigate = useNavigate();
  useEffect(() => {          
    
    //redirect to todolist page  
    if (isLogin) navigate('/todos', { replace: true });
  
  }, [isLogin]);

  const handleSubmit = (event) => {
    event.preventDefault();    
    
    const user = {
        email: email,
        password: password,
    };   

    const userService = new UserService(import.meta.env.VITE_API_URL);
    
    userService.login(user).then((response) => {

      console.log('response', response);
      const token = response.token

      //remove older cookie
      document.cookie = "token=; path=/; expires=Thu, 01 Jan 1970 00:00:00 UTC;";

      //set token in cookie
      document.cookie = `token=${token}; path=/;`;

      //save user connexion in local storage
      localStorage.setItem('user', JSON.stringify(response.user));

      //set isLogin to true
      setIsLogin(true);      
        
    });
  };

  return (
    <>    
   
    <form onSubmit={handleSubmit}>
    <p>
        try with : kuku@kuku.com / 12345
    </p>
      <label>
        Username:
        <input type="text" value={email} onChange={handleUsernameChange} />
      </label>
      <br />
      <label>
        Password:
        <input type="password" value={password} onChange={handlePasswordChange} />
      </label>
      <br />
      <button type="submit">Submit</button>
      <br/> OR
      <p>    
        <a href={`${import.meta.env.VITE_API_URL}/auth/google`}>
        <img with="50px" height="50px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/Google_%22G%22_logo.svg/1024px-Google_%22G%22_logo.svg.png" alt="" />
        Connect with Google
        </a>
      </p>
    </form>  
    

    </>
  );
}

export default Login;
