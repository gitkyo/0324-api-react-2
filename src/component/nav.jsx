import localforage from 'localforage';
import React from 'react';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';

function Navigation() {

  const checkLogin = () => {
    // if cookie with token
    const cookies = document.cookie.split(';');
    let tokenCookie = cookies.find((cookie) => cookie.includes('token'));
   
    if (localStorage.getItem('user') || tokenCookie) {
      return true;
    }
    return false;
  }

  const handleLogout = () => {
    localStorage.removeItem('user');
    
    //remove older cookie
    document.cookie = "token=; path=/; expires=Thu, 01 Jan 1970 00:00:00 UTC;";

    //clean localforage    
    localforage.clear();

    //redirect to login page
    const navigate = useNavigate();
    navigate('/login', { replace: true });
  }

  return (
    <nav className="navigation">
      <ul className="navigation__list">
        <li>
          <Link to="/">Home</Link>
        </li>
        {/* <li>
          <Link to="/about">About</Link>
        </li> */}
        <li>
          <Link to="/contact">Contact</Link>
        </li>

        {/* //if login  */}
        {checkLogin() && (
          <>
          <li>
            <Link to="#" onClick={handleLogout}>Logout</Link>
          </li>
          <li>
            <Link to="/todos">Todo list</Link>
          </li>
          </>
        )}

        {/* //if not login  */}
        {!checkLogin() && (
          <li>
            <Link to="/login">Login</Link>
          </li>
        )}

             
        
      </ul>
    </nav>
  );
}

export default Navigation;
